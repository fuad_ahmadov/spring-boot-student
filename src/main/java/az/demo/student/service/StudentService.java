package az.demo.student.service;

import az.demo.student.constant.ErrorEnum;
import az.demo.student.entity.Student;
import az.demo.student.exception.NotFoundException;
import az.demo.student.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    public Student byId(Long id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(ErrorEnum.STUDENT_NOT_FOUND, "Student not found, id : " + id));
    }

    public List<Student> all() {
        return studentRepository.findAll();
    }

    public Student create(Student student) {
        return studentRepository.save(student);
    }

    public Student update(Student student) {
        byId(student.getId());
        return studentRepository.save(student);
    }

    public void delete(Long id) {
        byId(id);
        studentRepository.deleteById(id);
    }
}
