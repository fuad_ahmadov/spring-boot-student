package az.demo.student.exception;

import az.demo.student.constant.ErrorEnum;

public class NotFoundException extends RuntimeException {

    private final ErrorEnum error;

    public NotFoundException(ErrorEnum error, String message) {
        super(message);
        this.error = error;
    }

    public ErrorEnum getError() {
        return error;
    }
}
