package az.demo.student.exception;


import az.demo.student.constant.ErrorEnum;
import az.demo.student.util.LocaleMessageResolver;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@Slf4j
@RequiredArgsConstructor
@RestControllerAdvice
public class GlobalExceptionHandler {

    private final LocaleMessageResolver messageResolver;

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ErrorDetail notFoundException(NotFoundException e) {
        log.error(e.getMessage(), e);
        return new ErrorDetail(e.getError().getCode(), resolveMessage(e.getError().getMessage()));
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorDetail handleInternalServerErrors(Exception e) {
        log.error(e.getMessage(), e);
        var errorEnum = ErrorEnum.INTERNAL_EXCEPTION;
        return new ErrorDetail(errorEnum.getCode(), resolveMessage(errorEnum.getMessage()));
    }

    private String resolveMessage(String messageCode) {
        try {
            return messageResolver.resolve(messageCode);
        } catch (NoSuchMessageException ex) {
            return messageResolver.resolve(ErrorEnum.INTERNAL_EXCEPTION.getMessage());
        }
    }
}