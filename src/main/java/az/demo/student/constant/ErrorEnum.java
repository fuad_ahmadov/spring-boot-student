package az.demo.student.constant;

public enum ErrorEnum {

    STUDENT_NOT_FOUND(1, "error.student.notfound"),
    INTERNAL_EXCEPTION(2, "error.server");

    private final Integer code;
    private final String message;

    ErrorEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
